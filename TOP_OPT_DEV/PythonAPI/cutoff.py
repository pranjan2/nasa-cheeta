#!/usr/bin/env python

from __future__ import print_function
import numpy as np
import readwriteData as rwd

#################################################################################################3

inp="gebracketmil"

outputCompliance="Obj_iterations.dat"


rhoPhys=rwd.getRhoPhys()
cutoff=0.8
coord="coordinates.msh"
map="mapelements.msh"


rwd.postprocess(coord,map,"output.inp",rhoPhys,cutoff)
