#!/usr/bin/env python

from __future__ import print_function
import numpy as np
import readwriteData as rwd

#################################################################################################3


def cgx_postopt(inp, meshFile,outputInp,outputMesh, rhoFile, cutoff):
    rhoPhys=rwd.getRhoPhys()
    outputfile=outputMesh

    with open(meshFile, "r") as f:
            lines = f.readlines()

    with open(outputMesh, "w") as new_f:
      #  new_f.write(lines[0])
        for linenum, line in enumerate(lines):  
            new_f.write(line)
            b=line.split(",")
            if b[0] == '*ELEMENT':
                break

               # print(int(b[0]))
           # if rhoPhys[int(b[0])-1] > cutoff :       
           #     new_f.write(line)
        for line in lines[linenum+1:]:  
            b=line.split(",")
               # print(int(b[0]))
            if rhoPhys[int(b[0])-1] > cutoff :       
                new_f.write(line)
    #print(linenum)
    
    l1="*HEADING\n"
    l2="Optimized output\n\n"

    l3="\n*INCLUDE,INPUT="+outputMesh

    with open(outputInp, "w") as text_file:
            text_file.writelines([l1,l2,l3])
            print("writing final output done!")


if __name__ == "__main__":

    inp="cantilever.inp"
    meshFile = 'mesh.nam'
    outputInp = 'cantOpt.inp'
    outputMesh = 'mesh_opt.nam'
    cutoff=0.3
    rhoFile = 'rhos.dat'

    cgx_postopt(inp, meshFile,outputInp,outputMesh, rhoFile, cutoff)
