import numpy as np
import pandas as pd

def su2ccx(description, filename, tags, element, set, load,FLAGS):
    """
    To convert SU2 FEA mesh into calculix files.
    Writing inp file is optional, and may require manual modifications

    Ghanendra K Das, CDILab, UIUC, 2021
    Last modified: 13 Nov, 2021
    """
    ################################################################################   
    with open(filename['su2file'],"r") as outfile:
        data = outfile.readlines()

    print("File loaded in memory...")
    print("Retreiving line numbers for tags. Please wait...")

    ################################################################################
    # Get line number for each tags, node and elements default
    tag_linenumber = np.zeros(np.size(tags), dtype='int')
    NUMTAG = np.zeros(np.size(tags), dtype='int')

    for linenum, line in enumerate(data):
        if 'NELEM' in line:        
            linesplit = line.split()
            NUMELEMENTS = int(linesplit[1])
            NELEMlinenumber=linenum + 1
            print('.....Number of ELEMENTS:', NUMELEMENTS)
            print(".....Set NELEM starts at line", NELEMlinenumber, "and has lines", NUMELEMENTS)

        elif 'NPOIN' in line:        
            linesplit = line.split()
            NUMNODES = int(linesplit[1])
            NPOINlinenumber=linenum + 1
            print('.....Number of NODES:', NUMNODES)
            print(".....Set NPOIN starts at line", NPOINlinenumber, "and has lines", NUMNODES)

        else:
            for tag_index, tag_name in enumerate(tags):
                if tag_name in line:
                    linesplit = line.split()
                    tag_linenumber[tag_index] = linenum + 2

                    nextlinedata = data[linenum+1]
                    splitnexline = nextlinedata.split()
                    NUMTAG[tag_index] = int(splitnexline[1])
                    print(".....Set ", tag_name, "starts at line", tag_linenumber[tag_index], "and has lines", NUMTAG[tag_index])
    del data       
    print("Tag line numbers obtained.\nExtracting data sections for elements and nodes. Please wait...") 
    #################################################################################

    # GET ELEMENT CONNECTIVITY AND ARRANGE
    DATA1 = pd.read_csv(filename['su2file'], header=None, delimiter="\s+", skiprows = NELEMlinenumber , nrows=NUMELEMENTS)
    DATA1_numpy = DATA1.to_numpy()

    EL_INDEX =DATA1_numpy[:, -1] +1     # Element Index
    EL_INDEX = EL_INDEX.T 
    EL_CONNECT = DATA1_numpy[:,1:-1]+1  # Element connectivity
    NELEDIME = np.size(EL_CONNECT[0,:])
    del DATA1, DATA1_numpy
    #################################################################################

    # GET COORDINATE VALUES AND ARRANGE
    DATA2 = pd.read_csv(filename['su2file'], header=None, delimiter="\s+", skiprows = NPOINlinenumber , nrows=NUMNODES)
    DATA2_numpy = DATA2.to_numpy()
    NO_INDEX =DATA2_numpy[:, -1] +1     # Coordinate Index
    NO_COORD = DATA2_numpy[:,0:-1]      # Coordinate values
    NDIME = np.size(NO_COORD[0,:])
    del DATA2, DATA2_numpy

    #################################################################################

    # Write node and elements to the MESH FILE
    if NDIME == 2:  # 2D Elements
        with open(filename['meshfile'],"w") as file:
            # Write node coordinates
            file.write("**This file containts mesh information\n")
            file.write("*NODE, NSET="+set['nodes'])
            for inode, value in enumerate(NO_INDEX):               
                file.write("\n"+str(int(value))+","+"{:.6e}" .format(NO_COORD[inode,0])+","+"{:.6e}" .format(NO_COORD[inode,1]))

            # Write element connectivity
            file.write("\n\n*ELEMENT, TYPE="+element['type']+",ELSET="+set['elements'])
            if NELEDIME ==3:    # Tri
                for iel, value in enumerate(EL_INDEX):               
                    file.write("\n"+str(int(value))+","+str(EL_CONNECT[iel,0])+","+str(EL_CONNECT[iel,1])+","+str(EL_CONNECT[iel,2]))
            elif NELEDIME ==4:    # Plane stress/strain/shell
                for iel, value in enumerate(EL_INDEX):               
                    file.write("\n"+str(value)+","+str(EL_CONNECT[iel,0])+","+str(EL_CONNECT[iel,1])+","+str(EL_CONNECT[iel,2])+","+str(EL_CONNECT[iel,3]))
            file.write("\n")

    elif NDIME == 3:    # 3D Elements       
        with open(filename['meshfile'],"w") as file:
             # Write node coordinates            
            file.write("**This file containts mesh information\n")
            file.write("*NODE, NSET="+set['nodes'])
            for inode, value in enumerate(NO_INDEX):               
                file.write("\n"+str(int(value))+","+"{:.6e}" .format(NO_COORD[inode,0])+","+"{:.6e}" .format(NO_COORD[inode,1])+","+"{:.6e}" .format(NO_COORD[inode,2]))

            # Write element connectivity
            file.write("\n\n*ELEMENT, TYPE="+element['type']+",ELSET="+set['elements'])
            if NELEDIME ==4:    # Tet
                for iel, value in enumerate(EL_INDEX):               
                    file.write("\n"+str(int(value))+","+str(EL_CONNECT[iel,0])+","+str(EL_CONNECT[iel,1])+","+str(EL_CONNECT[iel,2])+","+str(EL_CONNECT[iel,3]))
            elif NELEDIME ==8:    # Hex
                for iel, value in enumerate(EL_INDEX):               
                    file.write("\n"+str(int(value)) +"," +str(EL_CONNECT[iel,0]) +","+str(EL_CONNECT[iel,1])+","+str(EL_CONNECT[iel,2])+","+str(EL_CONNECT[iel,3]) +","+str(EL_CONNECT[iel,4]) +"," +str(EL_CONNECT[iel,5]) +"," +str(EL_CONNECT[iel,6]) +"," +str(EL_CONNECT[iel,7]))
            file.write("\n")

    print(filename['meshfile'] + " written") 
#################################################################################

    # GET BOUNDARY CONDITION NODES FOR EACH SET AND ARRANGE
    for tag_index, tag_name in enumerate(tags):
        DATA3 = pd.read_csv(filename['su2file'], header=None, delimiter="\s+", skiprows = tag_linenumber[tag_index] , nrows=NUMTAG[tag_index])
        DATA3_numpy = DATA3.to_numpy()
        TAG_NODE = DATA3_numpy[:,1:] + 1      # Coordinate values
        TAG_NODE_UNIQUE = np.unique(TAG_NODE)
        del DATA3, DATA3_numpy

        tagfilename = tag_name + ".nam"
        with open(tagfilename,"w") as file:
            file.write("*NSET, NSET=" + tag_name)
            for inode in TAG_NODE_UNIQUE:
                file.write("\n"+str(inode)+",")
            file.write("\n")
            print(tagfilename + " written")    
#################################################################################

    # WRITE INP FILE if FLAG set to true   
    if FLAGS['WRITE_INP']:
        with open(filename['ccxdeck'],"w") as file:
            file.write("**"+description)  
            file.write("\n*INCLUDE, INPUT="+filename['meshfile'])
            for tag_index, tag_name in enumerate(tags):
                tagfilename = tag_name + ".nam"
                file.write("\n*INCLUDE, INPUT=" + tagfilename)

            file.write("\n\n**Add Boundary file")        
            file.write("\n*BOUNDARY") 
            file.write("\n"+tags[0]+",1,3")  

            if FLAGS['TOPOPT']:
                file.write("\n\n*NSET, NSET = N")        
                file.write("\n1") 

                file.write("\n\n*DESIGNVARIABLES, TYPE = COORDINATE")        
                file.write("\nN") 

            file.write("\n\n**Add material named EL with elastic properties")
            file.write("\n*MATERIAL,NAME=EL")
            file.write("\n*ELASTIC")
            file.write("\n"+element['E']+","+element['poisson'])

            file.write("\n\n*SOLID SECTION, ELSET="+set['elements']+", MATERIAL=EL")
            file.write("\n*DENSITY")
            file.write("\n"+element['density'])

            file.write("\n\n*STEP")        
            file.write("\n*STATIC")
            file.write("\n*CLOAD")        
            file.write("\n"+tags[1]+",1,"+ load['x'])         
            file.write("\n"+tags[1]+",2,"+ load['y'])
            file.write("\n"+tags[1]+",3,"+ load['z'])       

            file.write("\n\n*NODE FILE")        
            file.write("\nU")
            file.write("\n*EL FILE")        
            file.write("\nS")            
            file.write("\n*NODE PRINT, NSET="+set['nodes'])        
            file.write("\nU")
            file.write("\n*EL PRINT,ELSET="+set['elements'])        
            file.write("\nS")            
            file.write("\n*END STEP") 

            if FLAGS['TOPOPT']:  
                file.write("\n\n*STEP")        
                file.write("\n*SENSITIVITY")
                file.write("\n*OBJECTIVE")        
                file.write("\nSTRAINENERGY")                      
                file.write("\n*END STEP")                 

    print("Complete!")         
   

if __name__ == "__main__":
    
    description = 'Cantilever test file for su2-ccx script'   # Add some details for inp file header, keep one line
    filename={}
    filename['su2file'] = "cantilever_32X32X63.su2"   # SU2 mesh file
    filename['ccxdeck']="cantilever.inp"    # ccx inp file
    filename['meshfile']='mesh.nam'    # ccx mesh file

    element = {}
    element['type']='C3D8'     # C3D4 = tetrahedral, C3D8 = hexahedral, CPS3 = tri, CPS4 = quad
    element['E']='69e9'     # Young's Modulus
    element['poisson']='0.33'   # Poisson ratio
    element['density']='2700'   # Density (optional for cload static case)

    # Note: For tags, add the names of the boundary node sets selected in su2. Add more sets if required
    # Note: By default, inp file assumes tags[0] =  fixed node set, tags[1] = load node sets. This order is assumed inside inp file 
    tags=['fixed', 'surface'] 

    set={}
    set['nodes'] = 'NALL'   #Set for all nodes
    set['elements'] = 'EALL'    #Set for all elements

    load={}     
    load['x'] = '0.0'     
    load['y'] = '0.1'
    load['z'] = '0.0'

    FLAGS = {}
    FLAGS['WRITE_INP'] = True

    FLAGS['TOPOPT'] = False # For topology optimization, additional flags are added
    su2ccx(description, filename, tags, element, set, load, FLAGS)