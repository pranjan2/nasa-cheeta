README

This directory contains all the source files for Calculix-Topology Optimization framework  for ccx_2.15

The executable is created as ccx_2.15_mod.

Compile: 
sudo make
sudo cp ccx_2.15_mod /usr/local/bin/




Run test file: 

ccx_2.15_mod -i INPUTDECK -p 3 -r 0.2 -v 0.5

-r default 0.0001
-v default 1



---> here -i FILENAME -p penaltyfortopology -r rmin -v volfrac
---> remove -p flag if sensitivity is not required. Typically for all p!=0, sensitivity is calculated.

3D  cantilever example: 3Dbeamtest\ccxfiles\
ccx_2.15_mod -i beam20p -p 3 -r 0.2 -v 0.5


*****NOTE**
To activate sensitivity calculation:
copy paste following section in inp file. 
NALL is the the name of the set of all nodes. If set of all nodes isnt defined, add (261 is total number of nodes)

create a random set of node:
*NSET,NSET=NNN  
96        (enter any node number randomly , preferably, not a BC)



->Then,add following section above 1st STEP section, probably for static calculation

*DESIGNVARIABLES, TYPE=COORDINATE
NNN



->Then, add following at the end:

*STEP
*SENSITIVITY
*OBJECTIVE
STRAINENERGY

*NODE FILE
SEN

*NODE PRINT, NSET=NALL   
**NODE FILE,
U

*END STEP

