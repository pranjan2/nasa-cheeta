function [C,Ceq,DC,DCeq]=nonlcon(rho,Volfrac,vole,Wbar)
%Constraints

rhophys=Wbar*rho; %Filter density



C=sum(rhophys'.*vole)-Volfrac*sum(vole); %Inequality constraint on volume
% DC=Wbar*vole';      %Convert filtered sensitivity back to actual
DC=Wbar*vole';
Ceq=[];
DCeq=[];

end
