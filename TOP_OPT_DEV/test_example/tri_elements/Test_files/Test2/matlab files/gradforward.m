function [grad,counter]=gradforward(f,x0,stepsize)
%.............................................................................
%   gradforward=calculates gradient at x0 using central finite difference method with h step size 
%   Input:
%       f,x0,h... function handle as a function of x0 vector, vector with points for grad estimation, step size. 
%       Default h=1e-8
%       Note: trigonometric functions in radian are accurate
%       d...number of function calls 
%Ghanendra K Das,AE, 2019, UIUC
%........................................................................
if nargin<3
    stepsize=1e-9;      %...Default Step size
end

%...Convert x to column vector
x0=x0(:);

%...Initialize gradient column vector
grad=zeros(max(size(x0)),1); 

%-------central difference method for gradient estimation-------------%
% f_xo=f(x0);  % f at x0
counter=0;  %Function call counter
%...Evaluate gradient wrt to each design variable by increment of h step
%size

for i=1:max(size(x0))
    h=zeros(max(size(x0)),1); %Initialize stepsize column vector
    h(i)=stepsize;      % Stepsize increment to ith design variable
    grad(i)=(f(x0+h)-f(x0-h))./(2*h(i));    %Evaluate gradient for ith variable
    counter=counter+2;
  if i<20
     disp(grad(i))
  end
end

end
