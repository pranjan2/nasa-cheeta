function [Fp,Ff,F]= Partition1D(F,fixeddof,freedof)
%.......................................
% Parition1D... Takes 1D row vector and paritions into fixed and free dof
% blocks
%........................................................................
% F=[1 2 3 4 5 6]

% fixeddof=[1 2 5];
% freedof=[3 4 6];


    F=F(:);
    
    Fp=F(fixeddof,1);
    Ff=F(freedof,1);
    
    F=[Fp;Ff];
   

end

