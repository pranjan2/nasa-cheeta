# -*- coding: mbcs -*-
#
# Abaqus/CAE Release 6.14-2 replay file
# Internal Version: 2014_08_22-09.00.46 134497
# Run by Manish Das on Thu Jul 09 06:28:18 2020
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=89.9033660888672, 
    height=120.99739074707)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from caeModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
    referenceRepresentation=ON)
s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=25.0)
g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
s.setPrimaryObject(option=STANDALONE)
s.rectangle(point1=(0.0, 0.0), point2=(5.875, 3.125))
s.ObliqueDimension(vertex1=v[1], vertex2=v[2], textPoint=(1.20028400421143, 
    3.71366167068481), value=8.0)
s.ObliqueDimension(vertex1=v[0], vertex2=v[1], textPoint=(-3.04297256469727, 
    2.2383713722229), value=1.0)
s.dragEntity(entity=v[0], points=((0.0, 2.125), (0.0, 2.125), (0.125, 0.5), (
    0.125, 0.125), (0.0, 0.0)))
p = mdb.models['Model-1'].Part(name='Part-1', dimensionality=THREE_D, 
    type=DEFORMABLE_BODY)
p = mdb.models['Model-1'].parts['Part-1']
p.BaseSolidExtrude(sketch=s, depth=1.0)
s.unsetPrimaryObject()
p = mdb.models['Model-1'].parts['Part-1']
session.viewports['Viewport: 1'].setValues(displayedObject=p)
del mdb.models['Model-1'].sketches['__profile__']
session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON, 
    engineeringFeatures=ON)
session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
    referenceRepresentation=OFF)
mdb.models['Model-1'].Material(name='Material-1')
mdb.models['Model-1'].materials['Material-1'].Elastic(table=((210000.0, 0.3), 
    ))
mdb.models['Model-1'].HomogeneousSolidSection(name='Section-1', 
    material='Material-1', thickness=None)
p = mdb.models['Model-1'].parts['Part-1']
c = p.cells
cells = c.getSequenceFromMask(mask=('[#1 ]', ), )
region = p.Set(cells=cells, name='Set-1')
p = mdb.models['Model-1'].parts['Part-1']
p.SectionAssignment(region=region, sectionName='Section-1', offset=0.0, 
    offsetType=MIDDLE_SURFACE, offsetField='', 
    thicknessAssignment=FROM_SECTION)
a = mdb.models['Model-1'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(
    optimizationTasks=OFF, geometricRestrictions=OFF, stopConditions=OFF)
a = mdb.models['Model-1'].rootAssembly
a.DatumCsysByDefault(CARTESIAN)
p = mdb.models['Model-1'].parts['Part-1']
a.Instance(name='Part-1-1', part=p, dependent=OFF)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(
    adaptiveMeshConstraints=ON)
mdb.models['Model-1'].StaticStep(name='Step-1', previous='Initial')
session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='Step-1')
session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=ON, 
    adaptiveMeshConstraints=OFF)
session.viewports['Viewport: 1'].assemblyDisplay.meshOptions.setValues(
    meshTechnique=ON)
elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
a = mdb.models['Model-1'].rootAssembly
c1 = a.instances['Part-1-1'].cells
cells1 = c1.getSequenceFromMask(mask=('[#1 ]', ), )
pickedRegions =(cells1, )
a.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2, 
    elemType3))
a = mdb.models['Model-1'].rootAssembly
e1 = a.instances['Part-1-1'].edges
pickedEdges = e1.getSequenceFromMask(mask=('[#c50 ]', ), )
a.seedEdgeByNumber(edges=pickedEdges, number=8, constraint=FINER)
a = mdb.models['Model-1'].rootAssembly
e1 = a.instances['Part-1-1'].edges
pickedEdges = e1.getSequenceFromMask(mask=('[#3af ]', ), )
a.seedEdgeByNumber(edges=pickedEdges, number=2, constraint=FINER)
a = mdb.models['Model-1'].rootAssembly
partInstances =(a.instances['Part-1-1'], )
a.generateMesh(regions=partInstances)
a = mdb.models['Model-1'].rootAssembly
n1 = a.instances['Part-1-1'].nodes
nodes1 = n1.getSequenceFromMask(mask=('[#1ef ]', ), )
a.Set(nodes=nodes1, name='loadnodes')
#: The set 'loadnodes' has been created (8 nodes).
session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=OFF, loads=ON, 
    bcs=ON, predefinedFields=ON, connectors=ON)
session.viewports['Viewport: 1'].assemblyDisplay.meshOptions.setValues(
    meshTechnique=OFF)
a = mdb.models['Model-1'].rootAssembly
region = a.sets['loadnodes']
mdb.models['Model-1'].ConcentratedForce(name='cloads', createStepName='Step-1', 
    region=region, cf2=1.0, distributionType=UNIFORM, field='', localCsys=None)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=ON, loads=OFF, 
    bcs=OFF, predefinedFields=OFF, connectors=OFF)
session.viewports['Viewport: 1'].assemblyDisplay.meshOptions.setValues(
    meshTechnique=ON)
a = mdb.models['Model-1'].rootAssembly
n1 = a.instances['Part-1-1'].nodes
nodes1 = n1.getSequenceFromMask(mask=('[#1ff ]', ), )
a.Set(nodes=nodes1, name='loadnodes')
#: The set 'loadnodes' has been edited (9 nodes).
session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=OFF, loads=ON, 
    bcs=ON, predefinedFields=ON, connectors=ON)
session.viewports['Viewport: 1'].assemblyDisplay.meshOptions.setValues(
    meshTechnique=OFF)
session.viewports['Viewport: 1'].view.setValues(nearPlane=12.6963, 
    farPlane=22.0943, width=6.76115, height=3.3634, cameraPosition=(-9.30343, 
    8.16001, 8.6821), cameraUpVector=(0.226541, 0.512621, -0.82819), 
    cameraTarget=(4.35175, 0.2813, 0.366954))
a = mdb.models['Model-1'].rootAssembly
f1 = a.instances['Part-1-1'].faces
faces1 = f1.getSequenceFromMask(mask=('[#1 ]', ), )
region = a.Set(faces=faces1, name='BBCC')
mdb.models['Model-1'].EncastreBC(name='BC-1', createStepName='Step-1', 
    region=region, localCsys=None)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF, 
    predefinedFields=OFF, connectors=OFF)
mdb.Job(name='beam3d', model='Model-1', description='', type=ANALYSIS, 
    atTime=None, waitMinutes=0, waitHours=0, queue=None, memory=90, 
    memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True, 
    explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, echoPrint=OFF, 
    modelPrint=OFF, contactPrint=OFF, historyPrint=OFF, userSubroutine='', 
    scratch='', resultsFormat=ODB, multiprocessingMode=DEFAULT, numCpus=1, 
    numGPUs=0)
mdb.jobs['beam3d'].submit(consistencyChecking=OFF)
#: The job input file "beam3d.inp" has been submitted for analysis.
#: Job beam3d: Analysis Input File Processor completed successfully.
#: Job beam3d: Abaqus/Standard completed successfully.
#: Job beam3d completed successfully. 
o3 = session.openOdb(name='C:/Temp/beam3d.odb')
#: Model: C:/Temp/beam3d.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     1
#: Number of Meshes:             1
#: Number of Element Sets:       3
#: Number of Node Sets:          4
#: Number of Steps:              1
session.viewports['Viewport: 1'].setValues(displayedObject=o3)
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_DEF, ))
session.viewports['Viewport: 1'].odbDisplay.commonOptions.setValues(
    deformationScaling=UNIFORM, uniformScaleFactor=1)
session.viewports['Viewport: 1'].odbDisplay.basicOptions.setValues(
    averagingThreshold=100)
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(INVARIANT, 
    'Magnitude'), )
a = mdb.models['Model-1'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=ON, bcs=ON, 
    predefinedFields=ON, connectors=ON)
session.viewports['Viewport: 1'].view.setValues(nearPlane=13.0264, 
    farPlane=21.8295, width=6.93692, height=3.45083, cameraPosition=(16.4453, 
    9.57119, 8.65869), cameraUpVector=(0.0750384, 0.138496, -0.987516), 
    cameraTarget=(3.71788, 0.246561, 0.36753))
session.viewports['Viewport: 1'].view.setValues(nearPlane=12.2917, 
    farPlane=22.5447, width=6.54566, height=3.2562, cameraPosition=(21.2418, 
    2.08251, -1.40104), cameraUpVector=(-0.467327, 0.56288, -0.681742), 
    cameraTarget=(3.609, 0.416552, 0.595882))
mdb.models['Model-1'].loads['cloads'].setValues(cf2=9.0, 
    distributionType=UNIFORM, field='')
session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF, 
    predefinedFields=OFF, connectors=OFF)
mdb.Job(name='Job-2', model='Model-1', description='', type=ANALYSIS, 
    atTime=None, waitMinutes=0, waitHours=0, queue=None, memory=90, 
    memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True, 
    explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, echoPrint=OFF, 
    modelPrint=OFF, contactPrint=OFF, historyPrint=OFF, userSubroutine='', 
    scratch='', resultsFormat=ODB, multiprocessingMode=DEFAULT, numCpus=1, 
    numGPUs=0)
mdb.jobs['Job-2'].submit(consistencyChecking=OFF)
#: The job input file "Job-2.inp" has been submitted for analysis.
#: Job Job-2: Analysis Input File Processor completed successfully.
#: Job Job-2: Abaqus/Standard completed successfully.
#: Job Job-2 completed successfully. 
o3 = session.openOdb(name='C:/Temp/Job-2.odb')
#: Model: C:/Temp/Job-2.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     1
#: Number of Meshes:             1
#: Number of Element Sets:       3
#: Number of Node Sets:          4
#: Number of Steps:              1
session.viewports['Viewport: 1'].setValues(displayedObject=o3)
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_DEF, ))
a = mdb.models['Model-1'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
del mdb.jobs['Job-2']
mdb.jobs['beam3d'].submit(consistencyChecking=OFF)
#: The job input file "beam3d.inp" has been submitted for analysis.
#: Job beam3d: Analysis Input File Processor completed successfully.
#: Job beam3d: Abaqus/Standard completed successfully.
#: Job beam3d completed successfully. 
o3 = session.openOdb(name='C:/Temp/beam3d.odb')
#: Model: C:/Temp/beam3d.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     1
#: Number of Meshes:             1
#: Number of Element Sets:       3
#: Number of Node Sets:          4
#: Number of Steps:              1
session.viewports['Viewport: 1'].setValues(displayedObject=o3)
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_DEF, ))
a = mdb.models['Model-1'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=ON, bcs=ON, 
    predefinedFields=ON, connectors=ON)
mdb.models['Model-1'].loads['cloads'].setValues(cf2=1.0, 
    distributionType=UNIFORM, field='')
session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF, 
    predefinedFields=OFF, connectors=OFF)
del mdb.jobs['beam3d']
mdb.Job(name='Job-1', model='Model-1', description='', type=ANALYSIS, 
    atTime=None, waitMinutes=0, waitHours=0, queue=None, memory=90, 
    memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True, 
    explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, echoPrint=OFF, 
    modelPrint=OFF, contactPrint=OFF, historyPrint=OFF, userSubroutine='', 
    scratch='', resultsFormat=ODB, multiprocessingMode=DEFAULT, numCpus=1, 
    numGPUs=0)
o3 = session.openOdb(name='C:/Temp/Job-1.odb')
#: Model: C:/Temp/Job-1.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     1
#: Number of Meshes:             1
#: Number of Element Sets:       3
#: Number of Node Sets:          4
#: Number of Steps:              1
session.viewports['Viewport: 1'].setValues(displayedObject=o3)
a = mdb.models['Model-1'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
mdb.jobs['Job-1'].submit(consistencyChecking=OFF)
#: The job input file "Job-1.inp" has been submitted for analysis.
#: Job Job-1: Analysis Input File Processor completed successfully.
#: Job Job-1: Abaqus/Standard completed successfully.
#: Job Job-1 completed successfully. 
o3 = session.openOdb(name='C:/Temp/Job-1.odb')
#: Model: C:/Temp/Job-1.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     1
#: Number of Meshes:             1
#: Number of Element Sets:       3
#: Number of Node Sets:          4
#: Number of Steps:              1
session.viewports['Viewport: 1'].setValues(displayedObject=o3)
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_DEF, ))
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(INVARIANT, 
    'Magnitude'), )
session.viewports['Viewport: 1'].odbDisplay.commonOptions.setValues(
    visibleEdges=FREE)
session.graphicsOptions.setValues(backgroundStyle=SOLID, 
    backgroundColor='#FFFFFF')
mdb.saveAs(
    pathName='E:/Academics/MS/NASA ULI/Calculix_code/ccx_2.15/Test_files/3Dbeamtest/3dbeamtest')
#: The model database has been saved to "E:\Academics\MS\NASA ULI\Calculix_code\ccx_2.15\Test_files\3Dbeamtest\3dbeamtest.cae".
session.viewports['Viewport: 1'].view.setValues(nearPlane=13.2933, 
    farPlane=22.2984, width=10.9164, height=5.43049, viewOffsetX=0.873549, 
    viewOffsetY=0.221085)
session.viewports['Viewport: 1'].view.setValues(nearPlane=14.3807, 
    farPlane=19.3272, width=11.8093, height=5.87467, cameraPosition=(4.50794, 
    7.34416, 15.913), cameraUpVector=(-0.0796217, 0.728413, -0.680496), 
    cameraTarget=(3.92881, 0.194209, -0.403336), viewOffsetX=0.944999, 
    viewOffsetY=0.239168)
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='S', outputPosition=INTEGRATION_POINT, refinement=(INVARIANT, 
    'Mises'), )
session.viewports['Viewport: 1'].view.setValues(nearPlane=13.5802, 
    farPlane=20.6238, width=11.152, height=5.54766, cameraPosition=(8.60052, 
    7.09642, 15.6144), cameraUpVector=(-0.338414, 0.725431, -0.599354), 
    cameraTarget=(3.83234, 0.102613, -0.0709873), viewOffsetX=0.892396, 
    viewOffsetY=0.225855)
session.viewports['Viewport: 1'].view.setValues(nearPlane=13.6929, 
    farPlane=20.2255, width=11.2445, height=5.59368, cameraPosition=(8.46275, 
    0.328182, 16.8613), cameraUpVector=(-0.115999, 0.945358, -0.3047), 
    cameraTarget=(3.72506, 0.493623, -0.320259), viewOffsetX=0.899799, 
    viewOffsetY=0.227729)
session.viewports['Viewport: 1'].view.setValues(nearPlane=12.3893, 
    farPlane=22.9792, width=10.174, height=5.06117, cameraPosition=(17.5615, 
    6.32003, 10.2835), cameraUpVector=(-0.276327, 0.718801, -0.637942), 
    cameraTarget=(3.65416, 0.696729, 0.658421), viewOffsetX=0.814139, 
    viewOffsetY=0.206049)
session.viewports['Viewport: 1'].view.setValues(nearPlane=13.1929, 
    farPlane=21.6567, width=10.8339, height=5.38943, cameraPosition=(12.6564, 
    8.34615, 13.4627), cameraUpVector=(-0.352676, 0.6893, -0.63284), 
    cameraTarget=(3.49778, 0.326917, 0.443903), viewOffsetX=0.866943, 
    viewOffsetY=0.219413)
session.viewports['Viewport: 1'].view.setValues(nearPlane=12.9842, 
    farPlane=21.8656, width=10.6625, height=5.30417, cameraPosition=(13.2987, 
    6.87394, 13.816), cameraUpVector=(-0.363286, 0.752956, -0.548708), 
    cameraTarget=(3.50909, 0.301113, 0.450385), viewOffsetX=0.853228, 
    viewOffsetY=0.215942)
session.viewports['Viewport: 1'].view.setValues(nearPlane=13.0072, 
    farPlane=21.8426, width=10.6814, height=5.31358, viewOffsetX=1.41921, 
    viewOffsetY=-1.04674)
session.viewports['Viewport: 1'].view.setValues(nearPlane=13.3421, 
    farPlane=21.5077, width=8.041, height=4.00007, viewOffsetX=0.644591, 
    viewOffsetY=-0.469709)
session.viewports['Viewport: 1'].view.setValues(nearPlane=13.5934, 
    farPlane=21.2156, width=8.19241, height=4.07539, cameraPosition=(12.2915, 
    6.54027, 14.5879), cameraUpVector=(-0.386189, 0.762653, -0.518863), 
    cameraTarget=(3.48438, 0.23938, 0.431237), viewOffsetX=0.656729, 
    viewOffsetY=-0.478553)
session.viewports['Viewport: 1'].view.setValues(nearPlane=13.5782, 
    farPlane=21.2307, width=8.18327, height=4.07084, viewOffsetX=0.547884, 
    viewOffsetY=-0.486361)
session.viewports['Viewport: 1'].view.setValues(nearPlane=13.5781, 
    farPlane=21.2308, width=8.1832, height=4.07081, viewOffsetX=0.531247, 
    viewOffsetY=-0.36123)
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(INVARIANT, 
    'Magnitude'), )
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(COMPONENT, 'U2'), )
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='S', outputPosition=INTEGRATION_POINT, refinement=(INVARIANT, 
    'Mises'), )
mdb.save()
#: The model database has been saved to "E:\Academics\MS\NASA ULI\Calculix_code\ccx_2.15\Test_files\3Dbeamtest\3dbeamtest.cae".
