function [uobj,grad,U]=PlaneStress_adjoint(rho,numElements,numNodes,fixednodes,t,E0,mu,COORD,map,p,F)
% %------------------------------------------------------------------------%

%Ghanendra Kumar Das,AE, UIUC

%------------------------------------------------------------------------%

                 

dofIndex=1:2*numNodes;                      %Index of all dofs

Emin=1e-9;
 

% -------Material Properties---------

E=E0/(1-mu^2)*[1 mu 0;                      %Plane stress stiffness matrix
              mu 1 0;
             0 0 (1-mu)/2];

%%

% [K,U,F]=InitializeFE(numNodes,2);           % 2D Initialization

K=zeros(numNodes*2,numNodes*2);
U=sparse(numNodes*2,1);
% F=sparse(numNodes*2,1);

% Fg=F; %For gravity load
ke0(:,:,numElements)=zeros(8);
ke(:,:,numElements)=zeros(8);
%----------Begin calculating K matrix for FE Analysis--------------%
for i=1:numElements
   nodes=map(i,:);   %extract nodes for each element from connectivity matrix
   gcoord=COORD(nodes,:);    %extract corresponding (x,y) coordinates for the nodes
   thickness=t;
   rhoi=rho(i);

   [~,B1,~,det_J1]=shapefunction2D(-sqrt(1/3),-sqrt(1/3),gcoord);  %Evaluate Integral function at 1st Gaussian point in 2x2 point rule
   [~,B2,~,det_J2]=shapefunction2D(sqrt(1/3),-sqrt(1/3),gcoord);   %Evaluate Integral function at 2nd Gaussian point in 2x2 point rule
   [~,B3,~,det_J3]=shapefunction2D(sqrt(1/3),sqrt(1/3),gcoord);    %Evaluate Integral function at 3rd Gaussian point in 2x2 point rule
   [~,B4,~,det_J4]=shapefunction2D(-sqrt(1/3),sqrt(1/3),gcoord);   %%Evaluate Integral function at 4rth Gaussian point in 2x2 point rule
   ke01=thickness*(B1'*Emin*B1*det_J1+B2'*Emin*B2*det_J2+B3'*Emin*B3*det_J3+B4'*Emin*B4*det_J4); %Evaluate total integral via Gauss rule, weights w1=w2=1       %partial diff ke wrt ti
   ke0(:,:,i)=thickness*(B1'*(E-Emin)*B1*det_J1+B2'*(E-Emin)*B2*det_J2+B3'*(E-Emin)*B3*det_J3+B4'*(E-Emin)*B4*det_J4); %Evaluate total integral via Gauss rule, weights w1=w2=1       %partial diff ke wrt ti
 

    ke(:,:,i)=ke01+rhoi^p*ke0(:,:,i);
%     else
%     ke(:,:,i)=rhoc^p*ke01+rhoi*rhoc^p*ke0(:,:,i);
%     end   
   index=[2*nodes-1;           %corresponding u,v dofs for the current nodes
        2*nodes];
   index=index(:);

   K(index,index)=K(index,index)+ke(:,:,i);     %Insert local ke in Global K matrix at correct position
%    Fg(index,1)=Fg(index,1)+rhoi*(mrho*g*volume_el)*[0;1/4;0;1/4;0;1/4;0;1/4];

end

% fixednodes=[mat(:,1), mat(:,end)];     %Fixednodes: Leftmost nodes in 2D arrangements                      
fixednodes=fixednodes(:)';
fixeddof=[2*fixednodes-1,2*fixednodes];
U(fixeddof,1)=0;                %Apply fixed value to fixed dofs
freedof=setdiff(dofIndex,fixeddof);

% Objnode=Loadnodes;
% 
% % Fc=[-100000/(0.1*0.1)]; %point load;
% % F=Fg;
% F(Objnode*2,1)=F(Objnode*2,1)+Fc;
Kff=sparse(K(freedof,freedof));
U(freedof,1)=Kff\F(freedof);
%.....Calculate state variable values using FE.............%
% Uf=Kff\(Ff-Kfp*Up);
% Fp=Kpf*Uf+Kpp*Up;
% U(freedof)=Uf;
% F(fixeddof)=Fp;

%......Objective is deflection at bottom right corner..............%
% objnode=2481;
% uobj=abs(U(objnode*2,1));

%.......Calculate partial diff of objective function wrt state
%variables...%
% L=zeros(2*numNodes,1);
% L(2*objnode)=abs(U(2*objnode))/U(2*objnode);



% Caclulate adjoint vector once for each f function

% psi(fixeddof,1)=0;
% psi(freedof,1)=-u(freedof,1);

%...Calculate gradient for ith design variable associated with ith element
grad=zeros(numElements,1);
uobj=0;
for i=1:numElements
    nodes=map(i,:);   %extract nodes for each element from connectivity matrix
   index=[2*nodes-1;           %corresponding u,v dofs for the current nodes
        2*nodes];
    index=index(:);
    
    rhoi=rho(i);
    keoi=ke0(:,:,i);
    kei=ke(:,:,i);
    ui=U(index,1);
    uobj=uobj+ui'*kei*ui;
    grad(i)=-p*rhoi^(p-1)*ui'*keoi*ui;
%  
%      if rhoi>=rhoc
%    grad(i)=2*ui'*(mrho*g*volume_el)*[0;1/4;0;1/4;0;1/4;0;1/4]*0.1*0.1*0.1-p*rhoi^(p-1)*ui'*keoi*ui;
%     else
%     grad(i)=2*ui'*(mrho*g*volume_el)*[0;1/4;0;1/4;0;1/4;0;1/4]*0.1*0.1*0.1-rhoc^p*ui'*keoi*ui;
%      end   
%      
    

%grad(i)=p*rhoi^(p-1)*mrho*g-p*rhoi^(p-1)*ui'*keoi*ui;


end

end


