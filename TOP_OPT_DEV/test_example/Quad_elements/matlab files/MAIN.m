clear ;close all;
%------------Prepare Domain---------%
%%
r=2;     %Filter radius
Volfrac=0.4;
p=3;
mrho=1;
 rhoc=0.2;
 g=-9.81*0;
Fc=-1;
E0=1;  
mu=0.3  ;    

disp('filter radius=')
disp(r)

%Total Length in X direction,m
totalLengthx=30;

%Tolotal Length in Y direction,m
totalHeight=20;     

%---------------------------------120x20 mesh-----------------------%
numElementsX=30;    %Number of Elements in X direction
numElementsY=20;     %Number of Elements in Y direction
t=1;  %thickness

numElements=numElementsX*numElementsY;      %Total number of Elements
numNodes=(numElementsX+1)*(numElementsY+1); %Total number of Elements
nodeIndex=1:numNodes;                       %Index of all nodes

dofIndex=1:2*numNodes;                      %Index of all dofs


eleLength=totalLengthx/numElementsX;        %Individual Element length in X direction 
eleWidth=totalHeight/numElementsY;          %Individual Element lenght in Y direction
                                  
disp('Generating node coordinates...')
x=0:eleLength:totalLengthx;
y=0:eleWidth:totalHeight;


[y,x]=meshgrid(y,x);
 
x=x(:);
y=y(:);
 
COORD=[x,y];                               %Coordinates of nodes bottom left to top right



%-----------Generate Element connectivity matrix map---------
disp('Generating 2D node arrangements...')
[mat,map]=Connectivity2D(nodeIndex,numElementsX,numElementsY);  % mat=2D node arrangement, map=2D conectivity matrix


disp('Calculating centriod of each element...') 
centroid=zeros(numElements,2);

for i=1:numElements
    nodes=map(i,:);   %extract nodes for each element from connectivity matrix
    gcoord=COORD(nodes,:);
    centroid(i,:)=mean(gcoord);
end
  
 disp('Generating density filter multiplier matrix...')
% W=zeros(numElements);


D=pdist(centroid);
rdist=squareform(D);
WW=(max(0,r-rdist));
row_sums=sum(WW,2);

Wbar=zeros(numElements); %Denstity filter matrix
for i=1:size(WW,2)
Wbar(i,:)=WW(i,:)/row_sums(i);
end

volume_el=eleLength*eleWidth*t;
vole=volume_el*ones(1,numElements);  %Volume of each element

Fullvol=Volfrac*sum(vole);      %Domain volume


% Loadnodes=60;
% fixednodes=[mat(:,1),mat(:,end)];

 Loadnodes=mat(1,end);
 fixednodes=[mat(:,1)];



%----------------Evaluate Optimal Area-------------------%
%%

rho=Volfrac*ones(numElements,1);     %design variables: rho 
lb=1e-4*ones(numElements,1);     % lower bound 
ub=ones(numElements,1);              %Upper limit

A=[];
b=[];
Aeq=[];
beq=[];



disp('Being optimization...')

% Define optimization options
options = optimoptions(@fmincon,'Algorithm','interior-point','MaxIter',5000,'MaxFunEvals',5000,'TolFun',1e-7,'TolX',1e-7,'GradObj','on','GradConstr','on','Display','iter','PlotFcn','optimplotfval'); %'PlotFcn','optimplotx' for design variables

f1=@(rho) fun(rho,Wbar,totalLengthx,numElementsX,totalHeight,numElementsY,t,E0,mu,COORD,mat,map,p,mrho,rhoc,g,Loadnodes,fixednodes,Fc,volume_el); %FE Analysis %Objective function
f2=@(rho)nonlcon(rho,Volfrac,vole,Wbar); %Gradient function

%Evaluate optimal value and optimal function
[rhoopt,fopt]=fmincon(f1,rho,A,b,Aeq,beq,lb,ub,f2,options);


rhoopt_Phys=Wbar*rhoopt;
rhomatrix=reshape(rhoopt_Phys,numElementsX,numElementsY);
rhomatrix=rhomatrix';
 rhomatrix=flip(rhomatrix);
figure
colormap(gray)
imagesc(-rhomatrix)
colorbar;
axis equal
V=sum(rhoopt_Phys'.*vole)/sum(vole);
title(['Optimized Physical density(filtered), C=',num2str(fopt), '   Volume=',num2str(V),'%'])

