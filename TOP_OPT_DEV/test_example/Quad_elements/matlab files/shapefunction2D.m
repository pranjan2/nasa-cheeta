function [N, B,J,det_J]=shapefunction2D(zeta,eta,gcoord)
%	shapefunction2D---Returns shape function N, B , Jacobian and determinant at given (zeta,eta) coordinate for element with gcoord nodal x,y coordinates
%
%       Ghanendra Kumar Das, 2019, AE, UIUC
%.........................................................................
%gcoord=global coordinate of the node

N=0.25*[(1-zeta)*(1-eta), (1+zeta)*(1-eta), (1+zeta)*(1+eta),(1-zeta)*(1+eta)]; %Shape functions

DN= 0.25*[-(1-eta) (1-eta) (1+eta) -(1+eta);        %Derivative of shape fuction wrt natural coordinates zeta,eta
        -(1-zeta) -(1+zeta) (1+zeta) (1-zeta)];
 
            
J=DN*gcoord;       %Jacobian of transformation
 
 det_J=det(J);      %determinant of Jacobian
 
 DG=J\DN;       %Derivative in global coordinates x,y
 
 B=zeros(3,8);      %B matrix

 B(1,[1 3 5 7])=DG(1,:);    %Assembly of B matrix
 B(2,[2 4 6 8])=DG(2,:);    
 B(3,[1 3 5 7])=DG(2,:);
 B(3,[2 4 6 8])=DG(1,:);
 
 N=[N(1) 0 N(2) 0 N(3) 0 N(4) 0;  %Expanded N matrix
    0 N(1) 0 N(2) 0 N(3) 0 N(4)];
end
