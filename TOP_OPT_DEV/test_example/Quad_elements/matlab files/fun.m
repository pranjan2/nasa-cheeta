function [Obj,gradd]=fun(rho,Wbar,totalLengthx,numElementsX,totalHeight,numElementsY,t,E0,mu,COORD,mat,map,p,mrho,rhoc,g,Loadnodes,fixednodes,Fc,volume_el)

% rho1=rho(1:totalLengthx*totalHeight,1);     %design variables: rho 
% rho2=rho(totalLengthx*totalHeight+1:end,1);     %design variables: rho

rhophys=Wbar*rho; %Filter density
% rhophys2=Wbar*rho2; %Filter density

% rhophys=[rhophys1;rhophys2];

[Obj,gradphys]=PlaneStress_adjoint(rhophys,totalLengthx,numElementsX,totalHeight,numElementsY,t,E0,mu,COORD,mat,map,p,mrho,rhoc,g,Loadnodes,fixednodes,Fc,volume_el);
gradd=Wbar*gradphys;  %Convert filtered sensitivity back to actual


%----For plotting each iteration-----% 
figure(1);

rhomatrix=reshape(rhophys,numElementsX,numElementsY);
rhomatrix=rhomatrix';
 rhomatrix=flip(rhomatrix);
colormap(gray)
imagesc(-rhomatrix)
axis equal
colorbar;
drawnow


end


