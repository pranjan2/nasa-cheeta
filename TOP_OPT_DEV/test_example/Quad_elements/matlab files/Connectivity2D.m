function [mat,map]=Connectivity2D(nodeIndex,numElementsX,numElementsY)
%	Connectivity2D- Takes 1D vector of node numbers, arranges them in 2D space %	and generates 2D connectivity matrix
%
%	Output:
%		mat,map.....2D arrangements of matrix, connectivity matrix
%       Ghanendra Kumar Das, 2019, AE, UIUC
%.........................................................................

mat=reshape(nodeIndex,numElementsX+1,numElementsY+1)';  %Arrange nodes in 2D array
map=zeros(numElementsX*numElementsY,4); %Initialize
counter=1;
    for ii=1:numElementsY
        for jj=1:numElementsX
           map(counter,:)=[mat(ii,jj) mat(ii,jj+1) mat(ii+1,jj+1) mat(ii+1,jj)];
               counter=counter+1;
        end
    end
end