%--------SDO AE498 HW 4,1a Q1
% Ghanendra Kumar Das, AE, UIUC

clc
clear ;close all;
format short
%----------------Evaluate Optimal Area-------------------%


disp('AE498:SDO, HW4, Q2(a)...GHANENDRA KUMAR DAS...')

%------------Prepare Domain---------%
r=0.2;     %Filter radius
disp('filter radius=')
disp(r)

%Total Length in X direction,m
totalLengthx=12 ;

%Tolotal Length in Y direction,m
totalHeight=2 ;     
     
%Young's Modulus, KN/m2
E0=75e3 ;  

%Poisson ratio
mu=0.3  ;    

%---------------------------------120x20 mesh-----------------------%
numElementsX=120;    %Number of Elements in X direction
numElementsY=20;     %Number of Elements in Y direction
t=0.1;  %thickness

numElements=numElementsX*numElementsY;      %Total number of Elements
numNodes=(numElementsX+1)*(numElementsY+1); %Total number of Elements
nodeIndex=1:numNodes;                       %Index of all nodes

dofIndex=1:2*numNodes;                      %Index of all dofs


eleLength=totalLengthx/numElementsX;        %Individual Element length in X direction 
eleWidth=totalHeight/numElementsY;          %Individual Element lenght in Y direction
                                  
disp('Generating node coordinates...')
x=0:eleLength:totalLengthx;
y=0:eleWidth:totalHeight;


[y,x]=meshgrid(y,x);
 
x=x(:);
y=y(:);
 
COORD=[x,y];                               %Coordinates of nodes bottom left to top right



%-----------Generate Element connectivity matrix map---------
disp('Generating 2D node arrangements...')
[mat,map]=Connectivity2D(nodeIndex,numElementsX,numElementsY);  % mat=2D node arrangement, map=2D conectivity matrix
                      
rhoPhys=0.4*ones(2400,1);     %design variables: rho 
disp('Calculating sensitivity using adjoint method...')
tic
[~,adj]=PlaneStress_adjoint(rhoPhys,totalLengthx,numElementsX,totalHeight,numElementsY,t,E0,mu,COORD,mat,map); %FE Analysis
 time_adjoint=toc

rho=0.4*ones(2400,1);     %design variables: rho 
%--------Using Finite forward difference
disp('calculating sensitivity using finite difference...')
tic
f1=@(rhoPhys)PlaneStress_adjoint(rhoPhys,totalLengthx,numElementsX,totalHeight,numElementsY,t,E0,mu,COORD,mat,map); %FE Analysis
finite_difference=gradforward(f1,rhoPhys,1e-9);
fd_time=toc

