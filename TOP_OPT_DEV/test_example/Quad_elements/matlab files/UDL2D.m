function L=UDL2D(P,a,b,face,gcoord)
%-------------------------------------------------------------------------% 
%Returns Load vector for pressure load along given face acting in range
%along length a to b

%Ghanendra Kumar Das, AE, UIUC
%-------------------------------------------------------------------------%

w=2*(b-a)/2;        %1 point Gauss weight

if face==1          %Determine integration points based on which face
    eta=-1;
    zeta=(b-a)/2*0+(b+a)/2;
    
elseif face==2
    zeta=1;
    eta=(b-a)/2*0+(b+a)/2;
    
elseif face==3
    eta=1;
    zeta=(b-a)/2*0+(b+a)/2;    

else
    zeta=-1;
    eta=(b-a)/2*0+(b+a)/2;
end
[N,~,J]=shapefunction2D(zeta,eta,gcoord);       %Evaluate shape function at Gauss point


if face==1 || face==3                           %Determine ds length based on Jacobian
    ds= sqrt(J(1,1)^2+J(1,2)^2);
    
else
    ds= sqrt(J(2,1)^2+J(2,2)^2);
end

L=w*N'*P'*ds;                                  %Perform Gauss integration to evaluate distributed load
end