import numpy as np
from pyoptsparse import OPT, Optimization, SLSQP, PSQP, CONMIN, IPOPT, ParOpt
from sys import path
path.append('.')
path.append('Functions/')
import pdb
optOptions = {}

def objfunc(xdict, **kwargs):
    x = xdict["xvars"]
    funcs = {}
    a1 = kwargs['a12'][0]
    a2 = kwargs['a12'][1]
    a3 = kwargs['a3']
    
    funcs["obj"] = a1*(x[1]-x[0]**2.)**2. + (a2-x[0])**2.
    g = [0.0]*2
    funcs["con"] = x[0]**2. + x[1]**2.0 - a3
    
    fail = False
    return funcs, fail



# Define problem dimensions

Dimension = 2



# Define bounds  ( box constraints ) on the DV

lower = -5.12*np.ones(Dimension)
upper =  5.12*np.ones(Dimension)

# Define initial candidate solution for the problem

initial = 4*np.ones(Dimension)
#initial = [3, -3]
# Define scaling parameters for design variables

Val = 1
scale = Val*np.ones(Dimension)

# Instantiate Optimization Problem ( Create optimization class)
optProb = Optimization(" Constrained Rosenbrock function", objfunc)

# Define design varibles
optProb.addVarGroup("xvars",Dimension,"c", lower = lower, upper = upper, value = initial, scale = scale)

# User-defined sensitivities
#sens = Rb.func_sens

optProb.addConGroup("con",1, upper = 0,scale=1.0)

#optProb.addConGroup("con",1, upper=5.12,lower = 2 ,scale=1)

optProb.addObj("obj")

# Arguments to pass into objfunc
a1 = 100.0
a2 = 1.0
a3 = 1.0

# Instantiate Optimizer (SLSQP) & Solve Problem
opt = SLSQP(options=optOptions)
opt.setOption('IPRINT',-2)



sol = opt(optProb,sens='FD',a12=[a1,a2],a3=a3)
print (sol)