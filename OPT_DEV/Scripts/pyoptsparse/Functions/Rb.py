import numpy as np
from scipy.optimize import fmin_slsqp

# Objective function

def func(xdict):
    x  = xdict["xvars"]
    funcs = {}
    funcs["obj"] = 100 * (x[1] - x[0] ** 2) ** 2 + (1 - x[0]) ** 2
    funcs["con"] = 1.1 - (x[0] - 2) **3 -x[1]

    fail = False
    return funcs, fail

############################################################################################

# Objective function gradients

def func_sens(xdict, funcsDict):
    x = xdict["xvars"]
    funcsSens = {}
    funcsSens["obj"] = {
        "xvars": [2 * 100 * (x[1] - x[0] ** 2) * (-2 * x[0]) - 2 * (1 - x[0]), 200*(x[1]-x[0]**2)]
    }
    funcsSens["con"] = {"xvars": [-3*(x[0] -2)**2, -1]
    }

    print(funcsSens)

    fail=False

    return funcsSens, fail
