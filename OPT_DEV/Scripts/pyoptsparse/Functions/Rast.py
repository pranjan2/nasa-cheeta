import numpy as np


# Objective function

def func(xdict):
    x  = xdict["xvars"]
    funcs = {}
    funcs["obj"] = (x[0]**2 - 10 * np.cos(2 * np.pi * x[0])) + \
  (x[1]**2 - 10 * np.cos(2 * np.pi * x[1])) + 20
    fail = False
    return funcs, fail


# Objective function gradients

#def func_sens(xdict, funcsDict):
#    x = xdict["xvars"]
#    funcsSens = {}
#    funcsSens["obj"] = {
#        "xvars": [2 * 100 * (x[1] - x[0] ** 2) * (-2 * x[0]) - 2 * (1 - x[0]), 200*(x[1]-x[0]**2)]
#    }

#    }

#    fail=False

#    return funcsSens, fail
