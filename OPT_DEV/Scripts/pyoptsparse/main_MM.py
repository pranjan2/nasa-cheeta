import numpy as np
from pyoptsparse import OPT, Optimization, SLSQP, PSQP, CONMIN, IPOPT, ParOpt
from sys import path
path.append('.')
path.append('Functions/')
import Himmel
optOptions = {}


# Define problem dimensions

Dimension = 2;

opt = SLSQP(options=optOptions)

# Define bounds  ( box constraints ) on the DV

lower = -6*np.ones(Dimension)
upper =  6*np.ones(Dimension)

# Define initial candidate solution for the problem

initial = 0*np.ones(Dimension)

Val = 0.001
scale = Val*np.ones(Dimension)

# Instantiate Optimization Problem ( Create optimization class)
optProb = Optimization(" Unconstrained Himmelblau's function", Himmel.func)

# Define design varibles
optProb.addVarGroup("xvars",Dimension,"c", lower = lower, upper = upper, value = initial, scale = scale)

# User-defined sensitivities
#sens = R

optProb.addObj("obj")

sol = opt(optProb, sens='FD', storeHistory="SLSQP_Himmel.hst")


print(sol)
