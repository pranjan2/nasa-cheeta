import matplotlib.pyplot as plt
import numpy as np
from sys import path
path.append('.')
path.append('Functions/')
import Rb

# First party modules
from pyoptsparse import History
from matplotlib import patheffects

db = {}
#opts = ["ipopt", "slsqp", "snopt", "conmin", "nlpqlp", "psqp"]
opts = ["IPOPT"]

for opt in opts:
    fileName = f"{opt}_Rosen.hst"
    try:
        db[opt] = History(fileName)
        print("History File Found for " f"{opt}")
    except FileNotFoundError:
        pass
        print("History File not found")

obj = {}
xuser = {}
for opt in db.keys():
    val = db[opt].getValues()
    obj[opt] = val["obj"]
    xuser[opt] = val["xvars"]


# Generate the Rosenbrock contours
delta = 0.1
x = np.arange(-5.12, 5.12, delta)
y = np.arange(-5.12, 5.12, delta)
X, Y = np.meshgrid(x, y)

# objective
Z = 100 * (Y - X ** 2) ** 2 + (1 - X) ** 2
# and the constraint contours
A = 1.1 - (X - 2) **3 -Y

#plt.figure()
#levels = [250, 500, 1000, 2000, 3000, 6000]
#CS = plt.contour(X, Y, Z, levels, colors="k")

pathx = xuser[opt][:,0]
pathy = xuser[opt][:,1]


fig,ax=plt.subplots(1,1)
cntr = ax.contour(X, Y, Z, [10,100,500,1000, 2000, 3000, 4000, 5000, 6000,8000,12000, 16000, 24000],cmap='RdBu_r')
ax.clabel(cntr, fmt="%2.1f", use_clabeltext=True)
ax.plot(pathx, pathy, color='green', marker='s', mfc='orange', mec='black', ms=6)

cg1 = ax.contour(X, Y, A, [0], colors='red')
plt.setp(cg1.collections,path_effects=[patheffects.withTickedStroke(angle=120, length=1)])


ax.set_title(f"{opt}", fontsize = 22, fontname ="Times New Roman")
ax.set_xlabel('$\mathregular{x_1}$', fontsize = 22, fontname="Times New Roman")
ax.set_ylabel('$\mathregular{x_2}$', fontsize = 22, fontname = "Times New Roman")

for axis in ['top', 'bottom', 'left', 'right']:
    ax.spines[axis].set_linewidth(1)

plt.xticks(fontname = "Times New Roman", fontsize = 20)
plt.yticks(fontname = "Times New Roman", fontsize = 20)

ax.tick_params(bottom=True, top=True, left=True, right=True)
ax.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False)

ax.tick_params(which='major', length=10, width=1.3, direction='in')
ax.tick_params(which='minor', length=5, width=1.3, direction='in')

F=plt.gcf()
Size = F.get_size_inches()
F.set_size_inches(Size[0]*1.3, Size[1]*1.3, forward=True)

plt.xlim([-5.5,5.5])
plt.ylim([-5.5,5.5])
plt.show()
