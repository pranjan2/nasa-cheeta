import numpy as np
from pyoptsparse import OPT, Optimization, SLSQP, PSQP, CONMIN, IPOPT, ParOpt
from sys import path
path.append('.')
path.append('Functions/')
import Rast
optOptions = {}


# Define problem dimensions

Dimension = 2;

opt = SLSQP(options=optOptions)

# Define bounds  ( box constraints ) on the DV

lower = -5.12*np.ones(Dimension)
upper =  5.12*np.ones(Dimension)

# Define initial candidate solution for the problem

initial = -5*np.ones(Dimension)

Val = 1
scale = Val*np.ones(Dimension)

# Instantiate Optimization Problem ( Create optimization class)
optProb = Optimization(" Unconstrained Rastigin function", Rast.func)

# Define design varibles
optProb.addVarGroup("xvars",Dimension,"c", lower = lower, upper = upper, value = initial, scale = scale)

# User-defined sensitivities
#sens = R

optProb.addObj("obj")

sol = opt(optProb, sens='FD', storeHistory="SLSQP_Rast.hst")


print(sol)
