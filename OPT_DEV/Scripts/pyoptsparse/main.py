import numpy as np
from pyoptsparse import OPT, Optimization, SLSQP, PSQP, CONMIN, IPOPT, ParOpt
from sys import path
path.append('.')
path.append('Functions/')
import Rb
optOptions = {}


# Define problem dimensions

Dimension = 2

opt = IPOPT(options=optOptions)

# Define bounds  ( box constraints ) on the DV

lower = -5.12*np.ones(Dimension)
upper =  5.12*np.ones(Dimension)

# Define initial candidate solution for the problem

initial = 4*np.ones(Dimension)
#initial = [3, -3]
# Define scaling parameters for design variables

Val = 1
scale = Val*np.ones(Dimension)

# Instantiate Optimization Problem ( Create optimization class)
optProb = Optimization(" Constrained Rosenbrock function", Rb.func)

# Define design varibles
optProb.addVarGroup("xvars",Dimension,"c", lower = lower, upper = upper, value = initial, scale = scale)

# User-defined sensitivities
sens = Rb.func_sens

optProb.addConGroup("con",1, upper = 0,scale=1.0)

#optProb.addConGroup("con",1, upper=5.12,lower = 2 ,scale=1)

optProb.addObj("obj")

# Create optimizer

sol = opt(optProb, sens=sens, storeHistory="IPOPT_Rosen.hst")
#sol = opt(optProb,sens = 'FD', storeHistory="PSQP_Rosen.hst")
print(sol)
