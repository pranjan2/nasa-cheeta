import matplotlib.pyplot as plt
import numpy as np
from sys import path
path.append('.')
path.append('Functions/')
import Himmel


# First party modules
from pyoptsparse import History
from matplotlib import patheffects


db = {}
#opts = ["ipopt", "slsqp", "snopt", "conmin", "nlpqlp", "psqp"]
opts = ["SLSQP"]


for opt in opts:
    fileName = f"{opt}_Himmel.hst"
    try:
        db[opt] = History(fileName)
        print("History File Found for" f"{opt}")
    except FileNotFoundError:
        pass
        print("History File not found")


obj = {}
xuser = {}
for opt in db.keys():
    val = db[opt].getValues()
    obj[opt] = val["obj"]
    xuser[opt] = val["xvars"]



# Generate the Rosenbrock contours
delta = 0.1
x = np.arange(-6, 6, delta)
y = np.arange(-6, 6, delta)
X, Y = np.meshgrid(x, y)

# objective
Z = (X**2 + Y -11)**2 + (X + Y**2 -7)**2

#plt.figure()
#levels = [250, 500, 1000, 2000, 3000, 6000]
#CS = plt.contour(X, Y, Z, levels, colors="k")

pathx = xuser[opt][:,0]
pathy = xuser[opt][:,1]


fig,ax=plt.subplots(1,1)
cntr = ax.contour(X, Y, Z, [0,10,50,100,150,200,250,300,400,500,600,700,800,1000,1200,1400,1600,2000],cmap='RdBu_r')

ax.clabel(cntr, fmt="%2.1f", use_clabeltext=True)
ax.plot(pathx, pathy, color='green', marker='s', mfc='orange', mec='black', ms=6)

ax.set_title(f"{opt}", fontsize = 22, fontname ="Times New Roman")
ax.set_xlabel('$\mathregular{x_1}$', fontsize = 22, fontname="Times New Roman")
ax.set_ylabel('$\mathregular{x_2}$', fontsize = 22, fontname = "Times New Roman")

for axis in ['top', 'bottom', 'left', 'right']:
    ax.spines[axis].set_linewidth(1)

plt.xticks(fontname = "Times New Roman", fontsize = 20)
plt.yticks(fontname = "Times New Roman", fontsize = 20)

ax.tick_params(bottom=True, top=True, left=True, right=True)
ax.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False)

ax.tick_params(which='major', length=10, width=1.3, direction='in')
ax.tick_params(which='minor', length=5, width=1.3, direction='in')

F=plt.gcf()
Size = F.get_size_inches()
F.set_size_inches(Size[0]*1.3, Size[1]*1.3, forward=True)

plt.xlim([-6,6])
plt.ylim([-6,6])
plt.show()
