import numpy as np
from scipy.optimize import fmin_slsqp

# Define n dimensional Rosenbrock function 

def rosen(x):

    Func = sum(100.0*(x[1:]-x[:-1]**2.0)**2.0 + (1-x[:-1])**2.0)

    return Func

############################################################################################

# Define function to compute the analytical gradients 

def rosen_der(x):

    xm = x[1:-1]

    xm_m1 = x[:-2]

    xm_p1 = x[2:]

    der = np.zeros_like(x)

    der[1:-1] = 200*(xm-xm_m1**2) - 400*(xm_p1 - xm**2)*xm - 2*(1-xm)

    der[0] = -400*x[0]*(x[1]-x[0]**2) - 2*(1-x[0])

    der[-1] = 200*(x[-1]-x[-2]**2)

    return der

############################################################################################


# Define initial candidate points 

x0=np.array([0.1, 1.7])

xb = []

# Define function handles for objective

#func = rosen(x0)

#fprime = rosen_der(x0)

# Define function handles for constraints

f_eqcons = []
f_ieqcons =[]
fprime_eqcons = []
fprime_ieqcons = []

its = 100
eps = 1.0e-04
accu = 1e-10

    # Run Optimizer
outputs = fmin_slsqp( x0             = x0             ,
                      func           = rosen          , 
                      f_eqcons       = f_eqcons       , 
                      f_ieqcons      = f_ieqcons      ,
                      fprime         = rosen_der      ,
                      fprime_eqcons  = fprime_eqcons  , 
                      fprime_ieqcons = fprime_ieqcons , 
                      args           = []             , 
                      bounds         = xb             ,
                      iter           = its            ,
                      iprint         = 2              ,
                      full_output    = True           ,
                      acc            = accu           ,
                      epsilon        = eps            )


