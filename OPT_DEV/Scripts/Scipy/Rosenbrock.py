# Script to compute the derivatives of n-dimensional Rosenbrock function 

import numpy as np
from scipy.optimize import minimize
from scipy.optimize import Bounds


# Define n dimensional Rosenbrock function 

def rosen(x):

    Func = sum(100.0*(x[1:]-x[:-1]**2.0)**2.0 + (1-x[:-1])**2.0)

    return Func

# Define function to compute the analytical gradients 

def rosen_der(x):

    xm = x[1:-1]

    xm_m1 = x[:-2]

    xm_p1 = x[2:]

    der = np.zeros_like(x)

    der[1:-1] = 200*(xm-xm_m1**2) - 400*(xm_p1 - xm**2)*xm - 2*(1-xm)

    der[0] = -400*x[0]*(x[1]-x[0]**2) - 2*(1-x[0])

    der[-1] = 200*(x[-1]-x[-2]**2)

    return der

# Define function to compute the full Hessian 

def rosen_hess(x):

    x = np.asarray(x)

    H = np.diag(-400*x[:-1],1) - np.diag(400*x[:-1],-1)

    diagonal = np.zeros_like(x)

    diagonal[0] = 1200*x[0]**2-400*x[1]+2

    diagonal[-1] = 200

    diagonal[1:-1] = 202 + 1200*x[1:-1]**2 - 400*x[2:]

    H = H + np.diag(diagonal)

    return H

# Define function to compute the Hessian product    

def rosen_hess_p(x, p):

    x = np.asarray(x)

    Hp = np.zeros_like(x)

    Hp[0] = (1200*x[0]**2 - 400*x[1] + 2)*p[0] - 400*x[0]*p[1]

    Hp[1:-1] = -400*x[:-2]*p[:-2]+(202+1200*x[1:-1]**2-400*x[2:])*p[1:-1] -400*x[1:-1]*p[2:]

    Hp[-1] = -400*x[-2]*p[-2] + 200*p[-1]

    return Hp    



# Initial candidate values 

x0=np.array([0.1, 1.7])

# Template : (func_name, DV vector, optimization algorithm, gradient vector, display options)

#res = minimize(rosen, x0, method='BFGS', jac=rosen_der, options={'disp': True})

#res = minimize(rosen, x0, method='Newton-CG', jac=rosen_der, hess=rosen_hess, options={'xtol': 1e-8, 'disp': True})

# Comment : The Newton-CG algorithm can be inefficient when the Hessian is ill-conditioned because of the poor quality search 
# directions provided by the method in those situations. The "trust-ncg" method deals more effectivelity with this problematic
# situation

# Full Hessian example

#res = minimize(rosen, x0, method='trust-ncg',jac=rosen_der, hess=rosen_hess,options={'gtol': 1e-8, 'disp': True})


# Hessian product example 
# res = minimize(rosen, x0, method='trust-ncg',jac=rosen_der, hessp=rosen_hess_p,options={'gtol': 1e-8, 'disp': True})


###### CONSTRAINED MULTI-VARIABLE OBJECTIVE FUNCTION 

# Define Inequality constraints

ineq_cons = {'type': 'ineq',

             'fun' : lambda x: np.array([1 - x[0] - 2*x[1],

                                         1 - x[0]**2 - x[1],

                                         1 - x[0]**2 + x[1]]),

             'jac' : lambda x: np.array([[-1.0, -2.0],

                                         [-2*x[0], -1.0],

                                         [-2*x[0], 1.0]])}


# Define Equality constraints

eq_cons = {'type': 'eq',

           'fun' : lambda x: np.array([2*x[0] + x[1] - 1]),

           'jac' : lambda x: np.array([2.0, 1.0])}


# Define bounds on DV

bounds = Bounds([0, -0.5], [1.0, 2.0])

#res = minimize(rosen, x0, method='SLSQP', jac=rosen_der,
#
 #              constraints=[eq_cons, ineq_cons], options={'ftol': 1e-9, 'disp': True},
#
#               bounds=bounds)


print(res.x)