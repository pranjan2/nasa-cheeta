import nlopt
import numpy as np
from numpy import *
import matplotlib.pyplot as plt
from matplotlib import patheffects
from numpy import linalg as LA
from pyOpt import Optimization

##############################################################################################################
#----------------------------------------OPTIMIZATION FUNCTIONS----------------------------------------------#
##############################################################################################################

Eval_count = 0
Grad_count = 0


# Define objective functions and gradients
def rosenbrock(x, grad):
    func = 0
    global Eval_count
    global Grad_count
    for  i in range(Ddimension-1):
        func += 100*(x[i+1] - x[i]**2)**2 + (1-x[i])**2
         
    if grad.size ==2:
        Grad_count+=1
        if (Analytical ==1):
            grad[0] = 400*(x[0]**3-x[0]*x[1]) + 2*(x[0]-1)
            grad[1] = 200*(x[1]-x[0]**2)

            if (Validate ==1):
                FD = np.zeros(Ddimension)
                FD[0] = fd(x,func,0)
                FD[1] = fd(x,func,1)  
                print( " Analytical : " ,grad[1], " FD : ", FD[1])    
        else:
            FD = np.zeros(Ddimension)
            for i in range(Ddimension):
                grad[i] = fd(x,func,i) 

        

    elif grad.size ==3:
        if (Analytical ==1):
            grad[0] = 100 * (4*x[0]**3-4*x[0]*x[1]) + (2*x[0]-2)
            grad[1] = 100 * ((2*x[1] -2 *x[0]**2) + ( 4* x[1]**3 -4 * x[2]*x[1])) + 2*(x[1]-2)      
            grad[2] = 200 * (x[2] - x[1]**2)

            if (Validate ==1):
                FD = np.zeros(Ddimension)
                for i in range(Ddimension):
                    FD[i] = fd(x,func,i)  
                print( " Analytical : " ,grad[1], " FD : ", FD[1])        
        
        else:
            FD = np.zeros(Ddimension)
            for i in range(Ddimension):
                grad[i] = fd(x,func,i)

    
    store_obj(func,Eval_count)
   # store_grad(grad,Eval_count)
    store_dv(x,Eval_count)
    Eval_count+=1            
    return func

def fd(x,f,i):
    h = 1e-08
    xp = np.copy(x)
    fp = 0
    xp[i] = xp[i] + h
    for j in range(0,Ddimension-1):
        fp += 100*(xp[j+1] - xp[j]**2)**2 + (1-xp[j])**2  
    deriv = (fp-f)/h
    return deriv


# Define constraints and their sensitivities

def constraint(x, grad):
    cons = 0
    for  i in range(Ddimension-1):
        cons += (1.1 -x[i]**3 + 8 + 6* x[i]**2 -12*x[i] - x[i+1])
    if grad.size ==2:
        grad[0] = -3*x[0]**2 + 12*x[0] -12 
        grad[1] = -1.0
    return cons

# Store objective function values to an array   

def store_obj(obj,Eval_count):
    global Evals
    Evals[Eval_count] = obj    

# Store gradient values to an array   

def store_grad(grad,Eval_count):
    global Gradient
    global Gnorm
    Gradient[0] = grad[0]
    Gradient[1] = grad[1]
    Norm = LA.norm(Gradient)
    Gnorm[Eval_count] = Norm

    



# Function to store design variable values to an array   

def store_dv(x,Eval_count):
    global x1,x2
    x1[Eval_count] = x[0]
    x2[Eval_count] = x[1]

def plot_convergence(Evals,Eval_count):
    x = np.arange(0,Eval_count)
    Local = Evals[0:Eval_count]
    Error = np.ones(Eval_count)
    for i in range(0,Eval_count-1):
        Error[i] = abs(Local[i+1]-Local[i])

    fig1 = plt.figure()
    ax1 = fig1.gca()
    plt.semilogy(x,Error)
    ax1.set_xlabel('Evaluation', fontsize = 22, fontname = "Times New Roman")
    ax1.set_ylabel('$\\epsilon_{abs}$', fontsize = 22, fontname="Times New Roman")

    for axis in ['top', 'bottom', 'left', 'right']:
        ax1.spines[axis].set_linewidth(1)

    plt.xticks(fontname = "Times New Roman", fontsize = 20)
    plt.yticks(fontname = "Times New Roman", fontsize = 20)

    ax1.tick_params(bottom=True, top=True, left=True, right=True)
    ax1.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False)
        
    ax1.tick_params(which='major', length=10, width=1.3, direction='in')
    ax1.tick_params(which='minor', length=5, width=1.3, direction='in')
    
   # plt.xlim([-0.0001,0.0001])
   # plt.ylim([-1,1])

    F=plt.gcf()
    Size = F.get_size_inches()
    F.set_size_inches(Size[0]*1.3, Size[1]*1.3, forward=True)
    plt.show()    


def plot_gnorm(Gnorm,Eval_count):
    x = np.arange(0,Eval_count)
    y = Gnorm[0:Eval_count]
   
    fig2 = plt.figure()
    ax2 = fig2.gca()
    plt.plot(x,y)
    
    ax2.set_xlabel('Evaluation', fontsize = 22, fontname = "Times New Roman")
    ax2.set_ylabel('Gnorm', fontsize = 22, fontname="Times New Roman")

    for axis in ['top', 'bottom', 'left', 'right']:
        ax2.spines[axis].set_linewidth(1)

    plt.xticks(fontname = "Times New Roman", fontsize = 20)
    plt.yticks(fontname = "Times New Roman", fontsize = 20)

    ax2.tick_params(bottom=True, top=True, left=True, right=True)
    ax2.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False)
        
    ax2.tick_params(which='major', length=10, width=1.3, direction='in')
    ax2.tick_params(which='minor', length=5, width=1.3, direction='in')

    F=plt.gcf()
    Size = F.get_size_inches()
    F.set_size_inches(Size[0]*1.3, Size[1]*1.3, forward=True)
    plt.show()    
        
        

#############################################################################################################
Ddimension = 2
Analytical = 1
Validate = 0
MaxEval = 5000
bound  = 5.12
initial = 4

Evals = np.empty(MaxEval)
Gnorm = np.empty(MaxEval)
Gradient = np.empty(Ddimension)
x1 = np.empty(MaxEval)
x2 = np.empty(MaxEval)
grad = np.zeros(Ddimension)
x = initial*np.ones(Ddimension)

# Select optimization algorithm and set design space dimension
#################################################
opt = nlopt.opt(nlopt.LD_MMA,Ddimension)




lower_val = -bound*np.ones(Ddimension)
upper_val =  bound*np.ones(Ddimension)

opt.set_lower_bounds(lower_val)
opt.set_upper_bounds(upper_val)

opt.set_min_objective(rosenbrock)
opt.add_inequality_constraint(lambda x,grad: constraint(x,grad), 1e-6)


opt.set_maxeval(MaxEval)
opt.set_xtol_rel(1e-6)

initial_point = initial*np.ones(Ddimension)

x = opt.optimize(initial_point)
minf = opt.last_optimum_value()

print("optimum at ", x)
print("minimum value = ", minf)
print("result code = ", opt.last_optimize_result())
print("Function Evaluations = ", Eval_count)
print("Gradient Evaluations = ", Grad_count)





#######################################################################################
#######################################################################################
#######################################################################################
if Ddimension==2:
    def design_space(bound,Ddimension,x1,x2):
        x1vec = np.linspace(-bound, bound, 1000)
        x2vec = np.linspace(-bound, bound, 1000)
        X, Y = np.meshgrid(x1vec, x2vec)
        obj = 100*(Y-X**2)**2 + (1-X)**2
        cons = (1.1 -X**3 + 8 + 6* X**2 -12*X- Y)

        fig,ax=plt.subplots(1,1)
        cntr = ax.contour(X, Y, obj, [10,100,500,1000, 2000, 3000, 4000, 5000, 6000,8000,12000, 16000, 24000],
                  cmap='RdBu_r')
        ax.clabel(cntr, fmt="%2.1f", use_clabeltext=True)
        cg1 = ax.contour(X, Y, cons, [0], colors='red')
        plt.setp(cg1.collections,path_effects=[patheffects.withTickedStroke(angle=120, length=1)]) 
        ax.plot(x1[:Eval_count], x2[:Eval_count], color='green', marker='s', mfc='orange', mec='black', ms=6)
   # fig.colorbar(cp) # Add a colorbar to a plot
        ax.set_title('MMA', fontsize = 22, fontname ="Times New Roman")

        ax.set_xlabel('$\mathregular{x_1}$', fontsize = 22, fontname="Times New Roman")
        ax.set_ylabel('$\mathregular{x_2}$', fontsize = 22, fontname = "Times New Roman")

        for axis in ['top', 'bottom', 'left', 'right']:
            ax.spines[axis].set_linewidth(1)

        plt.xticks(fontname = "Times New Roman", fontsize = 20)
        plt.yticks(fontname = "Times New Roman", fontsize = 20)

        ax.tick_params(bottom=True, top=True, left=True, right=True)
        ax.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False)
        
        ax.tick_params(which='major', length=10, width=1.3, direction='in')
        ax.tick_params(which='minor', length=5, width=1.3, direction='in')

        F=plt.gcf()
        Size = F.get_size_inches()
        F.set_size_inches(Size[0]*1.3, Size[1]*1.3, forward=True)

        plt.xlim([-5.5,5.5])
        plt.ylim([-5.5,5.5])
        plt.show()


#plot_convergence(Evals,Eval_count)

if Ddimension==2:
    design_space(bound,Ddimension,x1,x2)
    #plot_convergence(Evals,Eval_count)
    #plot_gnorm(Gnorm,Eval_count)

