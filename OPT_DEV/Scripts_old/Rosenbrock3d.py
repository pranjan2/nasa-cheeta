from pyOpt import Optimization
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import patheffects

from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D



#############################################################################################################
Ddimension = 2
Analytical = 1
Validate = 0
MaxEval = 5000
bound  = 5.12
initial = 4

Evals = np.empty(MaxEval)

x1 = np.empty(MaxEval)
x2 = np.empty(MaxEval)
x = initial*np.ones(Ddimension)

lower_val = -bound*np.ones(Ddimension)
upper_val =  bound*np.ones(Ddimension)


##############################################################################################################
#----------------------------------------OPTIMIZATION FUNCTIONS----------------------------------------------#
##############################################################################################################


Eval_count = 0
Grad_count = 0


# Define objective functions and gradients
def rosenbrock(x):
    f = 0
    global Eval_count
    for  i in range(Ddimension-1):
        f += 100*(x[i+1] - x[i]**2)**2 + (1-x[i])**2
    g = [0.0]*2
    for  i in range(Ddimension-1):
        g[0] += (1.1 -x[i]**3 + 8 + 6* x[i]**2 -12*x[i] - x[i+1])    
          
    fail = 0
    store_obj(f,Eval_count)
    store_dv(x,Eval_count)
    Eval_count+=1
    return f, g, fail 

# Store objective function values to an array   

def store_obj(obj,Eval_count):
    global Evals
    Evals[Eval_count] = obj

# Function to store design variable values to an array   

def store_dv(x,Eval_count):
    global x1,x2
    x1[Eval_count] = x[0]
    x2[Eval_count] = x[1]                        


def getlastsolution(prob: Optimization):
    new_index = prob.firstavailableindex(prob.getSolSet())
    return prob.getSol(new_index - 1)

# Instantiate Optimization Problem
opt_prob = Optimization('Rosenbrock Unconstraint Problem', rosenbrock)
opt_prob.addVarGroup('x',Ddimension, type='c', value=x, lower=lower_val, upper=upper_val)
opt_prob.addObj('f')
opt_prob.addCon('g','i')
#print(opt_prob)    


# Instantiate Optimizer (PSQP) & Solve Problem
from pyOpt import SLSQP

psqp =SLSQP()
psqp.setOption('IPRINT', 1)
#psqp.setOption('ACC', 1e-12)
#psqp.setOption('MAXIT', 500)
psqp.setOption('IOUT', 6)
psqp(opt_prob, sens_type='FD')
print(opt_prob.solution(0))


#######################################################################################
#######################################################################################
#######################################################################################
if Ddimension==2:
    def design_space(bound,Ddimension,x1,x2):
        x1vec = np.linspace(-bound, bound, 1000)
        x2vec = np.linspace(-bound, bound, 1000)
        X, Y = np.meshgrid(x1vec, x2vec)
        obj = 100*(Y-X**2)**2 + (1-X)**2
        cons = (1.1 -X**3 + 8 + 6* X**2 -12*X- Y)

        fig,ax=plt.subplots(1,1)
        cntr = ax.contour(X, Y, obj, [10,100,500,1000, 2000, 3000, 4000, 5000, 6000,8000,12000, 16000, 24000],
                  cmap='jet')
        ax.clabel(cntr, fmt="%2.1f", use_clabeltext=True)
        cg1 = ax.contour(X, Y, cons, [0], colors='red')
        plt.setp(cg1.collections,path_effects=[patheffects.withTickedStroke(angle=120, length=1)]) 
        ax.plot(x1[:Eval_count], x2[:Eval_count], color='green', marker='s', mfc='orange', mec='black', ms=6)
   # fig.colorbar(cp) # Add a colorbar to a plot
        ax.set_title('PSQP', fontsize = 22, fontname ="Times New Roman")
        ax.set_xlabel('$\mathregular{x_1}$', fontsize = 22, fontname="Times New Roman")
        ax.set_ylabel('$\mathregular{x_2}$', fontsize = 22, fontname = "Times New Roman")

        for axis in ['top', 'bottom', 'left', 'right']:
            ax.spines[axis].set_linewidth(1)

        plt.xticks(fontname = "Times New Roman", fontsize = 20)
        plt.yticks(fontname = "Times New Roman", fontsize = 20)

        ax.tick_params(bottom=True, top=True, left=True, right=True)
        ax.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False)
        
        ax.tick_params(which='major', length=10, width=1.3, direction='in')
        ax.tick_params(which='minor', length=5, width=1.3, direction='in')

        # Plot grid properties
       # ax.grid(which='major', color='black', linestyle='-', linewidth='0.2')
       # ax.minorticks_on()
       # ax.grid(which='minor', color='black', linestyle=':', linewidth='0.2')

        F=plt.gcf()
        Size = F.get_size_inches()
        F.set_size_inches(Size[0]*1.3, Size[1]*1.3, forward=True)

        plt.xlim([-5.5,5.5])
        plt.ylim([-5.5,5.5])
        plt.show()

def plotSurface(bound,Ddimension,x1,x2):
    x1vec = np.linspace(-bound, bound, 1000)
    x2vec = np.linspace(-bound, bound, 1000)
    X, Y = np.meshgrid(x1vec, x2vec)
    obj = 100*(Y-X**2)**2 + (1-X)**2
    cons = (1.1 -X**3 + 8 + 6* X**2 -12*X- Y)

    fig = plt.figure()
    ax = Axes3D(fig, auto_add_to_figure=False)
    fig.add_axes(ax)
    surf= ax.plot_surface(X, Y, obj, rstride=1, cstride=1, cmap='RdBu_r', linewidth=0,antialiased=False,alpha=1)
    #ax.plot_surface(X, Y, cons, rstride=1, cstride=1, cmap='autumn', linewidth = 0,antialiased=False)
     
    cset = ax.contour(X, Y, cons, 100, offset=-10, cmap='binary_r')
    #cset = ax.contour(X, Y, cons, zdir='z', offset=100, cmap='jet')


    ax.set_title('SLSQP', fontsize = 22, fontname ="Times New Roman")
    ax.set_xlabel('$\mathregular{x_1}$', fontsize = 22, fontname="Times New Roman")
    ax.set_ylabel('$\mathregular{x_2}$', fontsize = 22, fontname = "Times New Roman")
   
    
    ax.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
    ax.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
    ax.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))

    plt.xticks(fontname = "Times New Roman", fontsize = 20)
    plt.yticks(fontname = "Times New Roman", fontsize = 20)

   

    clb = plt.colorbar(surf, shrink=0.5, aspect=10)
    clb.ax.set_title('$f$',fontsize = 22, fontname = "Times New Roman")
    
    ax.w_zaxis.line.set_lw(0.)
    ax.set_zticks([])

    for l in clb.ax.yaxis.get_ticklabels():
        l.set_weight("bold")
        l.set_fontsize(12)


    ax.tick_params(bottom=True, top=True, left=True, right=True)
    ax.tick_params(labelbottom=True, labeltop=False, labelleft=True, labelright=False)
        
    ax.tick_params(which='major', length=10, width=1.3, direction='in')
    ax.tick_params(which='minor', length=5, width=1.3, direction='in')

    F=plt.gcf()
    Size = F.get_size_inches()
    F.set_size_inches(Size[0]*1.3, Size[1]*1.3, forward=True)
    #plt.xlim([-5.5,5.5])
    #plt.ylim([-5.5,5.5])
    #plt.zlim([-100,10000])

    ax.set_xlim3d([-5.5,5.5])
    ax.set_ylim3d([-5.5,5.5])
    ax.set_zlim3d([-1000,80000])
    plt.grid(b=None)
    ax.grid(False)
    plt.show()

if Ddimension==2:
    design_space(bound,Ddimension,x1,x2)
    #plotSurface(bound,Ddimension,x1,x2)
    print(" Function calls :", Eval_count)





