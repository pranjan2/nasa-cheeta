import nlopt
import numpy as np
from numpy import *
import matplotlib.pyplot as plt



# Specify the maximum number of evaluations
Max_eval = 10000

eval_count = 0

# Design space dimension 

Ddimension = 2


def myfunc(x, grad):
    if grad.size > 0:
        grad[0] = 400*(x[0]**3-x[0]*x[1]) + 2*(x[0]-1)
        grad[1] = 200*(x[1]-x[0]**2)
    obj = 100*(x[1]-x[0]**2)**2 + (1-x[0])**2
    global eval_count
    store_obj(obj,eval_count)
    store_dv(x,eval_count)
    eval_count = eval_count +1
    return obj

# Function to store objective function values to an array   

def store_obj(obj,eval_count):
    global Evals
    Evals = np.empty(Max_eval)
    Evals[eval_count] = obj

# Function to store design variable values to an array   

def store_dv(x,eval_count):
    global Evals
    global x1,x2
    x1 = np.empty(Max_eval)
    x1[eval_count] = x[0]

    x2 = np.empty(Max_eval)
    x2[eval_count] = x[1]

def initial_val(initial_point):
    x1 = initial_point[0]
    x2 = initial_point[1]
    obj_init = 100*(x2-x1**2)**2 + (1-x1)**2
    return obj_init



# Define constraints and their sensitivities

#def myconstraint(x, grad, a, b):
#    if grad.size > 0:
#        grad[0] = 3 * a * (a*x[0] + b)**2
#        grad[1] = -1.0
#    return (a*x[0] + b)**3 - x[1]


# Select optimization algorithm and set design space dimension
opt = nlopt.opt(nlopt.LD_MMA,Ddimension)

# Set design space bounds

lower_val = -10*np.ones(Ddimension)
upper_val =  10*np.ones(Ddimension)

opt.set_lower_bounds(lower_val)
opt.set_upper_bounds(upper_val)


opt.set_min_objective(myfunc)
#opt.add_inequality_constraint(lambda x,grad: myconstraint(x,grad,2,0), 1e-8)
#opt.add_inequality_constraint(lambda x,grad: myconstraint(x,grad,-1,1), 1e-8)


# Specify stopping criteria
opt.set_maxeval(Max_eval)
opt.set_xtol_rel(1e-6)

#Specify initial candidate points

initial_point = np.empty(Ddimension)
initial_point[0] = -9
initial_point[1] = -9 

x = opt.optimize([-initial_point[0], initial_point[1]])
minf = opt.last_optimum_value()

print("optimum at ", x[0], x[1])
print("minimum value = ", minf)
print("result code = ", opt.last_optimize_result())
print("Evaluations = ", eval_count)

Initial_Val =initial_val(initial_point) 
print(" Initial Value : ", Initial_Val)
print(" Final   Value : ", minf)

# Support functions

# Define objective funtion and its sensitivity



def plot_convergence(Evals,eval_count):
    x = np.arange(0,eval_count)
    y = Evals[0:eval_count]
    plt.plot(x,y)
    plt.show()


def design_space():
    x1vec = np.linspace(-10, 10, 1000)
    x2vec = np.linspace(-10, 10, 1000)
    X, Y = np.meshgrid(x1vec, x2vec)
    obj = 100*(Y-X**2)**2 + (1-X)**2

    fig,ax=plt.subplots(1,1)
    ax.contourf(X, Y,obj,100,shade=True, cmap = 'RdBu_r')
    ax.scatter(x[0], x[1])
   # fig.colorbar(cp) # Add a colorbar to a plot
    ax.set_title('Filled Contours Plot')
    ax.set_xlabel('x1')
    ax.set_ylabel('x2')
    plt.show()

def design_var(x1,x2):
    fig,ax=plt.subplots(1,1)
    plt.scatter(x1,x2)
    ax.set_xlim(-10, 10)
    ax.set_ylim(-10, 10)
    plt.show()

design_var(x1,x2)
design_space()
plot_convergence(Evals,eval_count)





